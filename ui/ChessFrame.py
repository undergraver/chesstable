from generated.ChessTableBase import *
import wx

class ChessFrame(ChessFrameBase):
    def __init__(self,parent=None):
        super().__init__(parent)

    def OnPaint(self,event):
        dc = wx.PaintDC(self)

        size = dc.GetSize()
        width = size.width
        height = size.height

        maxx = width - 1
        maxy = height - 1
        
        pen = dc.GetPen()
        penWidth = 4
        pen.SetColour(wx.RED)
        pen.SetWidth(penWidth)
        dc.SetPen(pen)

        self.DrawSquare(dc,10,10,100)

    def DrawSquare(self,dc, x, y, width):
        dc.DrawLine(x,y,x+width,y)
        dc.DrawLine(x+width,y,x+width,y+width)
        dc.DrawLine(x+width,y+width,x,y+width)
        dc.DrawLine(x,y+width,x,y)

    def OnExit(self,event):
        self.Close()
